﻿"Symbol";"Value";"Unit";
"SUPPLIER_NAME";"RS Components";"";
"TraceParts.PartVersion";"1.0.3";"";
"SUPPLIER";"Wurth Elektronik";"";
"TraceParts.PartStatus";"Published";"";
"TRACEPARTS";"TRACEPARTS";"";
"RS_STOCK_NUMBER";"8281597";"";
"REVISION";"A";"";
"DESIGN";"WR-PHD 2.54mm Pin Header THT Straight 5p";"";
"TraceParts.InstanceId";"10-29022016-125573B4X01TZGAQDDZLLMILM1D6V3I";"";
"TraceParts.PartLanguage";"en";"";
"TraceParts.PartTitle";"WR-PHD 2.54mm Pin Header THT Straight 5p";"";
"REFERENCE";"61300511121";"";
"TIMESTAMP";"19/2/2016 17:29:39";"";
"TraceParts.PartNumber";"10-29022016-125573";"";
"TraceParts.PartStatusId";"";"";
"TraceParts.ClassPath";"RS_COMPONENTS:/F_RS_COMPONENTS/PSSS_435563/PSS_435683/PSF_435704/10-29022016-125573";"";
