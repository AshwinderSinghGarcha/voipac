*PADS-LIBRARY-PCB-DECALS-V9*

SDCMF-10915W0T1  M 7.4   -20.385 0 2 6 0 17 9 0
TIMESTAMP 2016.11.24.15.28.46
-0.075 10.338 0     0 0.381 0.0381 1 0 34 "Regular <Romansim Stroke Font>"
REF-DES
-0.075 10.338 0     0 0.381 0.0381 1 32 35 "Regular <Romansim Stroke Font>"
PART-TYPE
CLOSED 5 0.254 27 -1
-13   23.42
13    23.42
13    -1.7 
-13   -1.7 
-13   23.42
OPEN   2 0.254 26 -1
-13   -1.7 
13    -1.7 
OPEN   2 0.254 26 -1
13    23.42
13    1.729
OPEN   2 0.254 26 -1
13    23.42
10.233 23.42
CIRCLE 2 0.254 26 -1
7.36284 25.673
7.58916 25.673
OPEN   2 0.254 26 -1
-13   20.015
-13   1.762
T6.875 23.92 6.875 23.92 1
T4.375 23.92 4.375 23.92 2
T1.875 23.92 1.875 23.92 3
T-0.625 23.92 -0.625 23.92 4
T-3.125 23.92 -3.125 23.92 5
T-5.625 23.92 -5.625 23.92 6
T-8.125 23.92 -8.125 23.92 7
T-9.75 23.92 -9.75 23.92 8
T9.375 23.92 9.375 23.92 9
T-11.05 23.92 -11.05 23.92 10
T-12.25 23.92 -12.25 23.92 11
T-13.65 21.767 -13.65 21.767 12
T11.45 21.5  11.45 21.5  13
T-13.65 0     -13.65 0     14
T13.65 0     13.65 0     15
T-11.5 21.12 -11.5 21.12 16
T9.5   20.77 9.5   20.77 17
PAD 0 3 N 0    
-2 1   RF  0   90   1.5 0  
-1 0   R  
0  0   R  
PAD 11 3 N 0    
-2 0.7 RF  0   90   1.5 0  
-1 0   R  
0  0   R  
PAD 10 3 N 0    
-2 0.7 RF  0   90   1.5 0  
-1 0   R  
0  0   R  
PAD 12 3 N 0    
-2 1.5 RF  0   90   2.8 0  
-1 0   R  
0  0   R  
PAD 13 3 N 0    
-2 1.5 RF  0   90   2.8 0  
-1 0   R  
0  0   R  
PAD 14 3 N 0    
-2 1.5 RF  0   90   2.8 0  
-1 0   R  
0  0   R  
PAD 15 3 N 0    
-2 1.5 RF  0   90   2.8 0  
-1 0   R  
0  0   R  
PAD 16 3 P 1.15 
-2 1.75 R  
-1 1.75 R  
0  1.75 R  
PAD 17 3 P 1.65 
-2 2.3 R  
-1 2.3 R  
0  2.3 R
*END*
