*PADS-LIBRARY-PART-TYPES-V9*

SDCMF-10915W0T1 SDCMF-10915W0T1 I CON 8 1 0 0 0
TIMESTAMP 2019.04.14.00.16.01
"Manufacturer_Name" MULTICOMP
"Manufacturer_Part_Number" SDCMF-10915W0T1
"Mouser Part Number" 
"Mouser Price/Stock" 
"RS Part Number" 
"RS Price/Stock" 
"Description" MULTICOMP - SDCMF-10915W0T1 - CONNECTOR, SD, METAL COVER, SMT, R/A
"Datasheet Link" http://www.farnell.com/datasheets/12099.pdf
GATE 1 9 0
SDCMF-10915W0T1
1 0 U 1
3 0 U 3
5 0 U 5
7 0 U 7
9 0 U 9
2 0 U 2
4 0 U 4
6 0 U 6
8 0 U 8

*END*
*REMARK* SamacSys ECAD Model
245479/130825/2.38/9/3/Connector
